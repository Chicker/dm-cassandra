#/bin/sh
docker stop cassandra.dm.local

docker build \
    -t dm/cassandra:0.1 \
    . \
&& docker run \
    -d \
    --net=host \
    --memory="1G" \
    -e "MAX_HEAP_SIZE=500M" \
    -e "HEAP_NEWSIZE=100M" \
    --rm=true \
    --name cassandra.dm.local \
    dm/cassandra:0.1 \
&& echo "Starting Cassandra. Please wait..."

while : ; do
    sleep 2;
    cmd=$(docker exec cassandra.dm.local bash -c 'nodetool status | grep "UN " && cqlsh --version | grep "cqlsh "')
    if [ $? -eq 0 ]
    then
        echo "Cassandra starting. OK"
        break
    else
        echo "..."
    fi
done

echo "Populating initial data ..."
while : ; do
    cmd=$(docker exec cassandra.dm.local bash -c 'cqlsh 127.0.0.1 -f /root/scripts/create_and_fill_db.cql')
    if [ $? -eq 0 ]
    then
        echo "Creating of initial structure of Cassandra DB. OK"
        break
    else
        echo "..."
        sleep 2
    fi
done
echo "Done."