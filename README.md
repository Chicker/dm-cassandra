# Automatic run

Just execute this command. It automatically starts and populates data for cassandra.

```
./cassandra-dm-restart.sh
```

# Manual run

## Build container

```
docker build \
    -t dm/cassandra:0.1 \
    .
```

## Run container

This container will be automatically removed when it stops.

```
docker run \
    -d \
    --net=host \
    --memory="1G" \
    -e "MAX_HEAP_SIZE=500M" \
    -e "HEAP_NEWSIZE=100M" \
    --rm=true \
    --name cassandra.dm.local \
    dm/cassandra:0.1
```

## Seeding initial data

```
docker exec \
    -it cassandra.dm.local \
    cqlsh localhost -f /root/scripts/create_and_fill_db.cql
```

## Jump to CQL shell (if you want to execute commands on cassanra)

```
docker exec -it cassandra.dm.local cqlsh localhost
```
